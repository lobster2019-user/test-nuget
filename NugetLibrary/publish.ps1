﻿Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"


function GetStringOrNumber
{
    Param ([string]$word)
    [int]$num = $null
    if([System.Int32]::TryParse($word, [ref] $num))
    {
        return $num
    }
    return $word
}

$apikey = Get-Content -Path "\\VBOXSVR\read\secrets\nuget.txt"
$config = "Debug"
$project = "MyNugetLibraryTest"


dotnet "pack"  "-c" $config  "$project.csproj" 
if ($LASTEXITCODE  -ne 0 ) { Write-Error "FAILED(pack): $LASTEXITCODE" }


$nugettemplate = "$project.*.nupkg"
Write-Host "pattern: $nugettemplate"

$currentdir = Split-Path $MyInvocation.MyCommand.Definition  -Parent
Write-host $currentdir

$nugetfiles = [System.IO.Directory]::GetFiles( [System.IO.Path]::Combine($currentdir, ".\bin\$config\"), $nugettemplate)
$nugetfiles2 = $nugetfiles `
        | ForEach-Object { [System.IO.Path]::GetFileNameWithoutExtension($_) } `
        | ForEach-Object { $_.Split('.')[0] + "." + `
                        [System.String]::Join(".",`
                                ($_.Split('.', 2)[1].Split('.') `
                             |  ForEach-Object {[System.String]::Format("{0:X08}", (GetStringOrNumber -word $_)) })`
                     ) }
$lastnuget = ($nugetfiles2 | Sort-Object  -Descending )[0]
$lastnugetindex = [Array]::IndexOf($nugetfiles2,$lastnuget)
$nugetpath = $nugetfiles[$lastnugetindex]

Write-Host "nuget: $nugetpath" 

dotnet "nuget" "push"  "$nugetpath"  -s "https://nuget.org" "--api-key" $apikey 
if ($LASTEXITCODE  -ne 0 ) { Write-Error "FAILED(push): $LASTEXITCODE" }

Write-Host "OK. $nugetpath" -ForegroundColor Green