﻿using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;

namespace SecretsTests
{
    public class Test
    {
        [Test]
        public void TestIni()
        {
            var dir = AppDomain.CurrentDomain.BaseDirectory;
            var configBuilder = new ConfigurationBuilder();


            configBuilder.AddUserSecrets("secrets");
            configBuilder.AddIniFile(Path.Combine(dir, "appsettings.ini"));


            var configuration = configBuilder.Build();
            var target = new ConfigOptions();
            configuration.Bind(target);

            Assert.AreEqual("value2", target.Prop1?.Prop2?.Prop3);
        }
        public class MyObject
        {
            public string _id { get; set; }
            public string Value { get; set; }

            public string[] Values { get; set; }
        }

        private LiteDB.LiteDatabase GetDatabase(LiteDB.FileMode fm)
        {
            var appdir = AppDomain.CurrentDomain.BaseDirectory;
            return new LiteDB.LiteDatabase(
          new LiteDB.ConnectionString
          {
              Filename =
               Path.Combine(appdir, "test.db"),
              Mode = fm,
              //Journal =false,
          });
        }
        [Test]
        public void LiteDbTest()
        {
            using (var db = GetDatabase(LiteDB.FileMode.Exclusive))
            {
                db.Mapper.RegisterType<MyObject>((o) =>
                {
                    var b = new LiteDB.BsonDocument { };
                    b.Add("_id", new LiteDB.BsonValue(o._id));
                    b.Add("Value", new LiteDB.BsonValue(o.Value));
                    b.Add("Values", new LiteDB.BsonArray(o.Values.Select(z=>new LiteDB.BsonValue(z)).ToList()));
                    return b;

                }, (b) =>
                {
                    return null;
                });

           //     using (var db2 = GetDatabase(LiteDB.ConnectionMode.Exclusive))
                {
                    db.DropCollection(nameof(MyObject));
                    var objectList = db.GetCollection<MyObject>();
              //      var objectList2 = db2.GetCollection<MyObject>();
               //     Console.WriteLine(objectList.Count());
                    objectList.Upsert(new MyObject
                    {
                        _id = "key-1",
                        Value = "value-1",
                        Values = new string[] { "1", "2" },
                    });


                    Console.WriteLine(objectList.Count());
                }
            }
        }
    }
}
