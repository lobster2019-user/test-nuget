﻿namespace SecretsTests
{
    public class ConfigOptions
    {
        public class Prop1Options
        {
            public Prop2Options Prop2 { get; set; }

            public class Prop2Options
            {
                public string Prop3 { get; set; }
            }
        }
        public Prop1Options Prop1 { get; set; }
    }
}
