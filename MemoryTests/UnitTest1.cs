using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(arg: 10000)]// 0.27 mb
        [TestCase(arg: 100000)]// 0.94 mb
        [TestCase(arg: 1000000)]//29 mb
        public void Test2_Int32(Int32 count)
        {
            var obj = new List<object>(count);
            var m1 = System.Environment.WorkingSet;
            for (var i = 0; i < count; ++i)
            {
                obj.Add(i);
            }
            var m2 = System.Environment.WorkingSet;
            Console.WriteLine($"int32 as obj [{count}] : {(m2 - m1) / 1024.0 / 1024.0} mb");
        }

        [TestCase(arg: 10000)]//  0.28 mb
        [TestCase(arg: 100000)]// 1 mb
        [TestCase(arg: 1000000)]//29  mb
        public void Test2_Int64(Int32 count)
        {
            var obj = new List<object>(count);
            var m1 = System.Environment.WorkingSet;
            for (var i = 0; i < count; ++i)
            {
                obj.Add((Int64)i);
            }
            var m2 = System.Environment.WorkingSet;
            Console.WriteLine($"int64 as obj [{count}] : {(m2 - m1) / 1024.0 / 1024.0} mb");
        }

        [TestCase(arg: 10000)]//  0.28 mb
        [TestCase(arg: 100000)]// 1 mb
        [TestCase(arg: 1000000)]//29  mb
        public void Test2_Ticks(Int32 count)
        {
            var obj = new List<object>(count);
            var m1 = System.Environment.WorkingSet;
            for (var i = 0; i < count; ++i)
            {
                obj.Add((Int64)i);
            }
            var m2 = System.Environment.WorkingSet;
            Console.WriteLine($"int64 as obj [{count}] : {(m2 - m1) / 1024.0 / 1024.0} mb");
        }


        [TestCase(arg: 10000)] // 0.68
        [TestCase(arg: 100000)] // 3.14
        [TestCase(arg: 1000000)]// 37.4
        public void Test2_Int32Str(Int32 count)
        {
            var obj = new List<string>(count);
            var m1 = System.Environment.WorkingSet;
            for (var i = 0; i < count; ++i)
            {
                obj.Add(i.ToString());
            }
            var m2 = System.Environment.WorkingSet;
            Console.WriteLine($"int32 as string [{count}] : {(m2 - m1) / 1024.0 / 1024.0} mb");
        }

        [Test]
        public void Test1()
        {
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "mapped.file.txt");
            System.IO.File.WriteAllText(file, "qwe");
            using (var mmf = MemoryMappedFile.CreateFromFile(
                                                    path: file,
                                                    mode: FileMode.OpenOrCreate))
            {
                var data =
                    System.Text.Encoding.UTF8.GetBytes(
                    DateTime.Now.ToString());

                

                using (var accessor = mmf.CreateViewStream())
                {
                    accessor.Write(data, 0, data.Length);
                    //accessor.SetLength(data.Length);
                    accessor.Flush();
                }
            }

            Assert.Pass();
        }

        [Test]
        public void Data()
        {
            DataTable t = new DataTable();
            t.Columns.Add(new DataColumn { DataType = typeof(TimeSpan), ColumnName = "Time" });
            var r = t.NewRow();
            //r["Time"] = 9999999999999999999L;
            t.Rows.Add(r);

            var t2 = new DateTime();
            t2 = t2.AddDays(1);
            Console.WriteLine(t2);
            Console.WriteLine($"time: {Marshal.SizeOf<TimeSpan>()}");
            Console.WriteLine($"datetime: {Unsafe.SizeOf<DateTime>()}");

            IDataReader r;
        }
    }
}