﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Tracing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Transport.Sockets.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace GeneratedWebAppNetCore
{
    //https://habr.com/ru/company/ozontech/blog/221037/
    //https://github.com/aspnet/AspNetCore/blob/master/src/Hosting/samples/SampleStartups/StartupExternallyControlled.cs
    public class Program
    {
        public class CustomListener
        {

        }
        public class ListenerObserver : IObserver<DiagnosticListener>,
            IObserver<KeyValuePair<string, object>>
        {
            private readonly ILogger<ListenerObserver> _logger;
            private readonly object _listener;
            private readonly List<IDisposable> _subscriptions
                 = new List<IDisposable>();

            public ListenerObserver(ILogger<ListenerObserver> logger,
                object listener)
            {
                _logger = logger;
                _listener = listener;
            }
            public void OnCompleted()
            {
                _logger.LogInformation("oncompleted");
                _subscriptions.ForEach(x => x.Dispose());
                _subscriptions.Clear();
            }

            public void OnError(Exception error)
            {
                _logger.LogError(error, "onerror");
            }

            public void OnNext(DiagnosticListener value)
            {
                _logger.LogInformation($"onnext {value.Name}");
                _subscriptions.Add(value.Subscribe(this, _ => true));
                _subscriptions.Add(value.SubscribeWithAdapter(_listener));
            }

            void IObserver<KeyValuePair<string, object>>.OnCompleted()
            {
                _logger.LogError("oncompleted");
            }

            void IObserver<KeyValuePair<string, object>>.OnError(Exception error)
            {
                _logger.LogError(error, message: "onerror");
            }

            void IObserver<KeyValuePair<string, object>>.OnNext(KeyValuePair<string, object> value)
            {
                _logger.LogInformation($"-------OnMext {value.Key} = {value.Value}");
            }
        }
        public class CustomEventListener : EventListener
        {
            protected override void OnEventSourceCreated(EventSource eventSource)
            {
                base.OnEventSourceCreated(eventSource);
            }
            string ToString(IReadOnlyCollection<object> o)
            {
                var list = o.ToArray();
                if(list.Length == 3)
                {
                    if(list[1] is string str && str == "LogBuffer" && list[2] is byte[] data)
                    {
                        return 
                            $"{list[0]},{list[1]},\r\n{System.Text.Encoding.UTF8.GetString(data)}";
                    }
                }
                return string.Join(",", list);
            }
            protected override void OnEventWritten(EventWrittenEventArgs eventData)
            {
                base.OnEventWritten(eventData);
                Console.WriteLine(
                    $@"

***net socket*** 
timestamp: {eventData.TimeStamp}
source: {eventData.EventSource}
name: {eventData.EventName}
msg: {eventData.Message}
id: {eventData.ActivityId}
opcode: {eventData.Opcode}
names: {string.Join(",", eventData.PayloadNames)}
eventid: {eventData.EventId}
tags: {eventData.Tags}
data:
{ToString(eventData.Payload)}
channel: {eventData.Channel}
version: 0x{eventData.Version.ToString("X02")}
eventTask: {eventData.Task}
id: {eventData.RelatedActivityId}
");
            }
        }
        public static void Main(string[] args)
        {
            var eventListener = new CustomEventListener();
            var eventSourceType =
                typeof(Socket).Assembly.GetType("System.Net.NetEventSource");
            var staticField = eventSourceType.GetField("Log",
                System.Reflection.BindingFlags.Static |
                System.Reflection.BindingFlags.Public);
            var netEventSource = (EventSource)staticField.GetValue(null);


            eventListener.EnableEvents(netEventSource,
                 EventLevel.LogAlways, EventKeywords.All, null);

            var consoleListener = new TextWriterTraceListener(Console.Out);
            

            var services = new ServiceCollection();
            services.AddLogging(logging => logging.AddConsole());
            var provider = services.BuildServiceProvider();

            /*
            using (var subscription =
                DiagnosticListener.AllListeners
                .Subscribe(new ListenerObserver(
                    logger: provider.GetRequiredService<ILogger<ListenerObserver>>(),
                listener: new CustomListener())))*/
            {
                var host = CreateWebHostBuilder(args).Build();
                host.Run();
            }


        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
                WebHost.CreateDefaultBuilder(args)
            .Configure(appBuilder =>
            {

            })
            .ConfigureServices((context, services) =>
            {

            })
            .ConfigureLogging(logging =>
            {

            })
            .ConfigureAppConfiguration(config =>
            {

            })
                .UseKestrel()
                .UseStartup<Startup>();
    }
}
