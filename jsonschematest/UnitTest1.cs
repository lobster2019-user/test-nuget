//using NUnit.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using NJsonSchema;
using NUnit.Framework;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using RangeAttribute = System.ComponentModel.DataAnnotations.RangeAttribute;

namespace Tests
{
    public enum EnumType
    {
        Undefined,
        Value1,
        Value2,
    }
    public class TesObject
    {
        [Required]
        public string RequiredField { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public EnumType EnumField { get; set; }

        [Range(10, 20)]
        public Int32 RangeField { get; set; }
    }
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
           
        }
    }
}