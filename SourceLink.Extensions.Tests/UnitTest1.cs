using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Tests
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var task = new SourceLink.Extensions.UpdateHostTask();
            task.IsLogEnabled = false;
            task.InputUrl = "ssh://git@gitlab/lobster2019-user/test-nuget.git";
            task.Execute();
            Assert.AreEqual("ssh://git@gitlab.com/lobster2019-user/test-nuget.git", task.OutputUrl);
        }

     
    }
}