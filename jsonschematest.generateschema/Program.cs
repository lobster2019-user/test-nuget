﻿using NJsonSchema;
using System;
using System.Threading.Tasks;
using Tests;

namespace jsonschematest.generateschema
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var schema = await JsonSchema4.FromTypeAsync<TesObject>();
            var schemaData = schema.ToJson();
            var schemaFile = System.IO.Path.Combine(System.AppContext.BaseDirectory,
                "../../../../jsonschematest/testobject.schema.json");
            System.IO.File.AppendAllText(schemaFile, schemaData);
        }
    }
}
