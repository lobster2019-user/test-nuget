﻿using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace SourceLink.Extensions
{
    public class UpdateSourceRootHostTask : Task
    {
        [Required]
        [Output]
        public ITaskItem[] SourceRoot { get; set; }

        [Required]
        public string FieldName { get; set; }

        public bool IsLogEnabled { get; set; } = true;

        public override bool Execute()
        {
            var result = true;
            foreach (var item in SourceRoot)
            {
                result = UpdateHostHelper.Execute(Log, IsLogEnabled, item.GetMetadata(FieldName), out var output) && result;
                //item.RemoveMetadata(FieldName);
                item.SetMetadata(FieldName, output);
            }
            return result;
        }
    }
}
