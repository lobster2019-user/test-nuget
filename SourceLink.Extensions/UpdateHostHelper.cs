﻿using System;
using System.Linq;
using Microsoft.Build.Utilities;

namespace SourceLink.Extensions
{
    public static class UpdateHostHelper
    {
        public static bool Execute(TaskLoggingHelper Log, bool IsLogEnabled, string InputUrl,out string OutputUrl)
        {
            OutputUrl = "";
            var path = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)
                , ".ssh", "config");

            if (IsLogEnabled)
            {
                Log.LogMessage("Execute!message");
                Log.LogWarning("Execute!warning;");
            }

            if (System.IO.File.Exists(path))
            {
                if (IsLogEnabled)
                {
                    Log.LogWarning($"found config file: {path}");
                }
                bool foundHost = false;
                bool foundHostname = false;
                var link = new UriBuilder(InputUrl);
                var host = link.Host;

                if (IsLogEnabled)
                {
                    Log.LogWarning($"Host={host}");
                }

                foreach (var line in System.IO.File.ReadAllLines(path))
                {
                    var line2 = line.Split('#')[0].Trim();
                    var parts = line2.Split(' ', '\t').Select(z => z.Trim()).Where(z => z.Length > 0).ToArray();
                    if (string.Equals(parts[0], "host", StringComparison.OrdinalIgnoreCase))
                    {
                        foundHost = false;
                        Log.LogWarning($"found host");
                        if (parts.Skip(1).Contains(host, StringComparer.OrdinalIgnoreCase))
                        {
                            Log.LogWarning($"found hostname");
                            foundHost = true;
                        }
                    }
                    else
                    {
                        if (foundHost)
                        {
                            if (parts.Length > 1)
                            {
                                if (string.Equals(parts[0], "hostname", StringComparison.OrdinalIgnoreCase))
                                {
                                    foundHostname = true;
                                    host = parts[1];
                                    break;
                                }
                            }
                        }
                    }
                }

                if (foundHostname)
                {
                    link.Host = host;
                    OutputUrl = link.ToString();
                    if (IsLogEnabled)
                    {
                        Log.LogWarning($"replace host {InputUrl} -> {OutputUrl}");
                    }
                }
                else
                {
                    OutputUrl = InputUrl;
                }
            }
            else
            {
                if (IsLogEnabled)
                {
                    Log.LogWarning($"not found config file: {path}");
                }
            }

            return true;
        }
    }
}
