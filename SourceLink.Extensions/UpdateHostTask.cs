﻿using System;
using System.Linq;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace SourceLink.Extensions
{
    public class UpdateHostTask : Task
    {
        [Required]

        public string InputUrl { get; set; }
        [Output]
        public string OutputUrl { get; set; }

        public bool IsLogEnabled { get; set; } = true;

        public override bool Execute()
        {
            var result = UpdateHostHelper.Execute(Log, IsLogEnabled, InputUrl, out var output);
            OutputUrl = output;
            return result;
        }
    }
}
